import os
import json
import sqlite3
from sqlite3 import Error
from flask import request
from flask import Flask, Response
app = Flask(__name__)


class JsonResponse(Response):
    def __init__(self, response, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.response = json.dumps(response)
        self.headers.add('content-type', 'application/json')


def create_connection(db_file='database.db'):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
 
    return conn
 
 
def select_all_todos(conn):
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute("SELECT * FROM todos")
 
    return [dict(item) for item in cur.fetchall()]


def save_todo(conn, title: str, content: str, order:int =100):
    cur = conn.cursor()
    q = cur.execute("INSERT INTO todos('title', 'content', 'order') VALUES ('{0}','{1}','{2}')".format(title, content, order))
    conn.commit()
    return {
        'id': cur.lastrowid,
        'title': title,
        'content': content,
        'order': order
    }
 
 
def select_todo_by_id(conn, id: int):
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute("SELECT * FROM todos WHERE id=?", (id,))
    result = cur.fetchone()
    if result:
        return dict(result)
    return None


@app.route('/todos/', methods=['GET', 'POST'])
def get_todos():
    conn = create_connection()
    
    if request.method == 'GET':
        return JsonResponse(select_all_todos(conn), status=200)
    if request.method == 'POST':
        if not all([
            'title' in request.json,
            'content' in request.json
        ]):
            return JsonResponse({'status': 'title and content is required'}, status=422)
        else:
            todo = save_todo(conn, request.json['title'], request.json['content'], request.json['order'] if 'order' in request.json else 100)

            return JsonResponse(todo, status=201)


@app.route('/todos/<int:id>', methods=['GET'])
def get_todo_by_id(id: int):
    conn = create_connection()
    if request.method == 'GET':
        todo = select_todo_by_id(conn, id)
        if todo is not None:
            return JsonResponse(todo, status=200)
        return JsonResponse({'error': 'Not found'}, status=404)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')
