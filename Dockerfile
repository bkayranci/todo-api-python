FROM python:3.7-alpine

LABEL maintainer="bkayranci@gmail.com"
LABEL Description="This image is a echo server"
EXPOSE 8000/tcp

WORKDIR /opt/todo-app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN python .docker/create_database.py
RUN rm -rf .docker

CMD [ "python", "./api.py" ]
