import os
import sqlite3

query = '''
CREATE TABLE "todos" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"title"	TEXT NOT NULL,
	"content"	TEXT NOT NULL,
	"order"	INTEGER NOT NULL DEFAULT 100
);
'''

database_file = 'database.db'

if not os.path.exists(database_file):
    w = open(database_file, 'w+')
    w.close()
    conn = sqlite3.connect(database_file)
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()
